
#include <Servo.h> // include Servo library 

// 180 horizontal MAX
Servo horizontal; // horizontal servo
int servoh = 180;   // 90;     // stand horizontal servo

int servohLimitHigh = 180;
int servohLimitLow = 55;

// 65 degrees MAX
Servo vertical;   // vertical servo 
int servov = 30;    //   90;     // stand vertical servo

int servovLimitHigh = 80;
int servovLimitLow = 5;


// LDR pin connections
//  name  = analogpin;
int ldrlt = 1; //LDR top left - BOTTOM LEFT    //<--- BDG
int ldrrt = 3; //LDR top rigt - BOTTOM RIGHT 
int ldrld = 2; //LDR down left - TOP LEFT
int ldrrd = 4; //ldr down rigt - TOP RIGHT

void setup()
{
  
  Serial.begin(9600);
  
// servo connections
// name.attacht(pin);
  horizontal.attach(9); 
  vertical.attach(10);
  horizontal.write(180);
  vertical.write(45);
  delay(3000); 
}

void loop() 
{
  int lt = analogRead(ldrlt); // top left
  int rt = analogRead(ldrrt); // top right
  int ld = analogRead(ldrld); // down left
  int rd = analogRead(ldrrd); // down rigt
  
   int dtime = analogRead(5)/20; // read potentiometers  
   int tol = analogRead(0)/4;
  //int dtime = 10;
  //int tol = 50;
  
  int avt = (lt + rt) / 2; // average value top
  int avd = (ld + rd) / 2; // average value down
  int avl = (lt + ld) / 2; // average value left
  int avr = (rt + rd) / 2; // average value right

  int dvert = avt - avd; // check the diffirence of up and down
  int dhoriz = avl - avr;// check the diffirence og left and rigt
  
  
  Serial.print("average top = ");
  Serial.print(avt);
  Serial.print("\n");
  Serial.print("average down = ");
  Serial.print(avd);
  Serial.print("\n");
  Serial.print("average left = ");
  Serial.print(avl);
  Serial.print("\n");
  Serial.print("average right = ");
  Serial.print(avr);
  Serial.print("\n\nspeed setting = ");
  Serial.print(dtime);
  Serial.print("\n");
  Serial.print("motion tolerance = ");
  Serial.print(tol);
  Serial.print(" ");
  Serial.print("\n");
  Serial.print("vertical servo position = ");
  Serial.print("\n");
  Serial.print("horizontal servo position = ");
  Serial.print("\n");
      
  if (-1*tol > dvert || dvert > tol) // check if the diffirence is in the tolerance else change vertical angle
  {
  if (avt > avd)
  {
    servov = servov-5;
    Serial.print("vertical servo increasing...\n");
     if (servov > servovLimitHigh) 
     { 
      servov = servovLimitHigh;
     }
  }
  else if (avt < avd)
  {
    servov= servov+5;
    Serial.print("vertical servo decreasing...\n");
    if (servov < servovLimitLow)
  {
    servov = servovLimitLow;
  }
  }
  vertical.write(servov);
  }
  
  if (-1*tol > dhoriz || dhoriz > tol) // check if the diffirence is in the tolerance else change horizontal angle
  {
  if (avl > avr)
  {
    servoh = servoh+5;
    Serial.print("horizontal servo decreasing...\n");
    if (servoh < servohLimitLow)
    {
    servoh = servohLimitLow;
    }
  }
  else if (avl < avr)
  {
    servoh = servoh-5;
    Serial.print("horizontal servo increasing...\n");
     if (servoh > servohLimitHigh)
     {
     servoh = servohLimitHigh;
     }
  }
  else if (avl = avr)
  {
    // nothing
  }
  horizontal.write(servoh);
  }
   delay(dtime*100);
  
  Serial.print("\n=======================================\n\n");
  delay(500);
}
